# Git Monorepo to Manyrepo Splitting Using splitsh-lite

Provides an environment containing a standard packaged version of Git, a customized version of the [splitsh-lite](https://github.com/splitsh/lite) utility, and shell scripts for automating some specific Git repository management tasks.

## Usage

Bitbucket Pipelines are supposed to use containers from this image for repository splitting and updating tasks, but they are sufficiently generic that the usual

```console
$ docker run -t -i --tmpfs /repo -w /repo  uadrupal/schismatic
```

should drop into a shell within a temporary working directory called `/repo`, and then the commands

```console
# git clone https://github.com/twigphp/Twig
# lite --prefix=lib/ --origin=refs/tags/v1.24.1 --path=Twig --scratch
```

would make a clone of the canonical remote Twig repository there, then run the *splitsh-lite* utility on this.

## Manual Building

The Dockerfile requires Docker 17.05 or later, and uses a multistage build, first compiling the splitsh-lite binary in a full Go development environment, then copying this into a smaller production image that nevertheless contains a full Git installation and some utility shell scripts. The normal `docker build` command should work as expected with this.
